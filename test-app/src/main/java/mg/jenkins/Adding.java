package mg.jenkins;

import mg.jesnkins.submodule1.SubModuleClass;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Adding implements CommandLineRunner {

    private final SubModuleClass adding = new SubModuleClass();

    public static void main(String[] args) {
        SpringApplication.run(Adding.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            final double no1 = Double.parseDouble(args[0]);
            final double no2 = Double.parseDouble(args[1]);

            System.out.println(adding.add(no1, no2));

            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }

    }
}
